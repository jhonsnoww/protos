//
//  Generated code. Do not modify.
//  source: auth.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use userDescriptor instead')
const User$json = {
  '1': 'User',
  '2': [
    {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    {'1': 'userName', '3': 2, '4': 1, '5': 9, '10': 'userName'},
    {'1': 'email', '3': 3, '4': 1, '5': 9, '10': 'email'},
    {'1': 'password', '3': 4, '4': 1, '5': 9, '10': 'password'},
    {'1': 'last_updated', '3': 5, '4': 1, '5': 9, '10': 'lastUpdated'},
    {'1': 'created_at', '3': 6, '4': 1, '5': 9, '10': 'createdAt'},
    {'1': 'is_active', '3': 7, '4': 1, '5': 8, '10': 'isActive'},
  ],
};

/// Descriptor for `User`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userDescriptor = $convert.base64Decode(
    'CgRVc2VyEg4KAmlkGAEgASgJUgJpZBIaCgh1c2VyTmFtZRgCIAEoCVIIdXNlck5hbWUSFAoFZW'
    '1haWwYAyABKAlSBWVtYWlsEhoKCHBhc3N3b3JkGAQgASgJUghwYXNzd29yZBIhCgxsYXN0X3Vw'
    'ZGF0ZWQYBSABKAlSC2xhc3RVcGRhdGVkEh0KCmNyZWF0ZWRfYXQYBiABKAlSCWNyZWF0ZWRBdB'
    'IbCglpc19hY3RpdmUYByABKAhSCGlzQWN0aXZl');

@$core.Deprecated('Use loginRequestDescriptor instead')
const LoginRequest$json = {
  '1': 'LoginRequest',
  '2': [
    {'1': 'emailOrUserName', '3': 1, '4': 1, '5': 9, '10': 'emailOrUserName'},
    {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `LoginRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginRequestDescriptor = $convert.base64Decode(
    'CgxMb2dpblJlcXVlc3QSKAoPZW1haWxPclVzZXJOYW1lGAEgASgJUg9lbWFpbE9yVXNlck5hbW'
    'USGgoIcGFzc3dvcmQYAiABKAlSCHBhc3N3b3Jk');

@$core.Deprecated('Use loginResponseDescriptor instead')
const LoginResponse$json = {
  '1': 'LoginResponse',
  '2': [
    {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
  ],
};

/// Descriptor for `LoginResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginResponseDescriptor = $convert.base64Decode(
    'Cg1Mb2dpblJlc3BvbnNlEhQKBXRva2VuGAEgASgJUgV0b2tlbg==');

@$core.Deprecated('Use authenticatedResponseDescriptor instead')
const AuthenticatedResponse$json = {
  '1': 'AuthenticatedResponse',
  '2': [
    {'1': 'isAuthenticated', '3': 1, '4': 1, '5': 8, '10': 'isAuthenticated'},
  ],
};

/// Descriptor for `AuthenticatedResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List authenticatedResponseDescriptor = $convert.base64Decode(
    'ChVBdXRoZW50aWNhdGVkUmVzcG9uc2USKAoPaXNBdXRoZW50aWNhdGVkGAEgASgIUg9pc0F1dG'
    'hlbnRpY2F0ZWQ=');

