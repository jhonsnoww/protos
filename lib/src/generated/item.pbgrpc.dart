//
//  Generated code. Do not modify.
//  source: item.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:async' as $async;
import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'package:protobuf/protobuf.dart' as $pb;

import 'item.pb.dart' as $1;

export 'item.pb.dart';

@$pb.GrpcServiceName('ItemService')
class ItemServiceClient extends $grpc.Client {
  static final _$getItem = $grpc.ClientMethod<$1.ItemsByIdRequest, $1.Item>(
      '/ItemService/getItem',
      ($1.ItemsByIdRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.Item.fromBuffer(value));
  static final _$createItem = $grpc.ClientMethod<$1.Item, $1.Item>(
      '/ItemService/createItem',
      ($1.Item value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.Item.fromBuffer(value));
  static final _$updateItem = $grpc.ClientMethod<$1.Item, $1.Item>(
      '/ItemService/updateItem',
      ($1.Item value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.Item.fromBuffer(value));
  static final _$deleteItem = $grpc.ClientMethod<$1.ItemsByIdRequest, $1.Item>(
      '/ItemService/deleteItem',
      ($1.ItemsByIdRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.Item.fromBuffer(value));
  static final _$getItems = $grpc.ClientMethod<$1.Empty, $1.ItemsByResponse>(
      '/ItemService/getItems',
      ($1.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.ItemsByResponse.fromBuffer(value));
  static final _$getItemsByToday = $grpc.ClientMethod<$1.ItemByTodayRequest, $1.ItemByCategoryResponse>(
      '/ItemService/getItemsByToday',
      ($1.ItemByTodayRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.ItemByCategoryResponse.fromBuffer(value));

  ItemServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options,
        interceptors: interceptors);

  $grpc.ResponseFuture<$1.Item> getItem($1.ItemsByIdRequest request, {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getItem, request, options: options);
  }

  $grpc.ResponseFuture<$1.Item> createItem($1.Item request, {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createItem, request, options: options);
  }

  $grpc.ResponseFuture<$1.Item> updateItem($1.Item request, {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateItem, request, options: options);
  }

  $grpc.ResponseFuture<$1.Item> deleteItem($1.ItemsByIdRequest request, {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteItem, request, options: options);
  }

  $grpc.ResponseStream<$1.ItemsByResponse> getItems($1.Empty request, {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$getItems, $async.Stream.fromIterable([request]), options: options);
  }

  $grpc.ResponseFuture<$1.ItemByCategoryResponse> getItemsByToday($1.ItemByTodayRequest request, {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getItemsByToday, request, options: options);
  }
}

@$pb.GrpcServiceName('ItemService')
abstract class ItemServiceBase extends $grpc.Service {
  $core.String get $name => 'ItemService';

  ItemServiceBase() {
    $addMethod($grpc.ServiceMethod<$1.ItemsByIdRequest, $1.Item>(
        'getItem',
        getItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.ItemsByIdRequest.fromBuffer(value),
        ($1.Item value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.Item, $1.Item>(
        'createItem',
        createItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.Item.fromBuffer(value),
        ($1.Item value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.Item, $1.Item>(
        'updateItem',
        updateItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.Item.fromBuffer(value),
        ($1.Item value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.ItemsByIdRequest, $1.Item>(
        'deleteItem',
        deleteItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.ItemsByIdRequest.fromBuffer(value),
        ($1.Item value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.Empty, $1.ItemsByResponse>(
        'getItems',
        getItems_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.Empty.fromBuffer(value),
        ($1.ItemsByResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.ItemByTodayRequest, $1.ItemByCategoryResponse>(
        'getItemsByToday',
        getItemsByToday_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.ItemByTodayRequest.fromBuffer(value),
        ($1.ItemByCategoryResponse value) => value.writeToBuffer()));
  }

  $async.Future<$1.Item> getItem_Pre($grpc.ServiceCall call, $async.Future<$1.ItemsByIdRequest> request) async {
    return getItem(call, await request);
  }

  $async.Future<$1.Item> createItem_Pre($grpc.ServiceCall call, $async.Future<$1.Item> request) async {
    return createItem(call, await request);
  }

  $async.Future<$1.Item> updateItem_Pre($grpc.ServiceCall call, $async.Future<$1.Item> request) async {
    return updateItem(call, await request);
  }

  $async.Future<$1.Item> deleteItem_Pre($grpc.ServiceCall call, $async.Future<$1.ItemsByIdRequest> request) async {
    return deleteItem(call, await request);
  }

  $async.Stream<$1.ItemsByResponse> getItems_Pre($grpc.ServiceCall call, $async.Future<$1.Empty> request) async* {
    yield* getItems(call, await request);
  }

  $async.Future<$1.ItemByCategoryResponse> getItemsByToday_Pre($grpc.ServiceCall call, $async.Future<$1.ItemByTodayRequest> request) async {
    return getItemsByToday(call, await request);
  }

  $async.Future<$1.Item> getItem($grpc.ServiceCall call, $1.ItemsByIdRequest request);
  $async.Future<$1.Item> createItem($grpc.ServiceCall call, $1.Item request);
  $async.Future<$1.Item> updateItem($grpc.ServiceCall call, $1.Item request);
  $async.Future<$1.Item> deleteItem($grpc.ServiceCall call, $1.ItemsByIdRequest request);
  $async.Stream<$1.ItemsByResponse> getItems($grpc.ServiceCall call, $1.Empty request);
  $async.Future<$1.ItemByCategoryResponse> getItemsByToday($grpc.ServiceCall call, $1.ItemByTodayRequest request);
}
