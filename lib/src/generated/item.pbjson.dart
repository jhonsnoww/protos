//
//  Generated code. Do not modify.
//  source: item.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use categoryDescriptor instead')
const Category$json = {
  '1': 'Category',
  '2': [
    {'1': 'INCOME', '2': 0},
    {'1': 'EXPENSE', '2': 1},
  ],
};

/// Descriptor for `Category`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List categoryDescriptor = $convert.base64Decode(
    'CghDYXRlZ29yeRIKCgZJTkNPTUUQABILCgdFWFBFTlNFEAE=');

@$core.Deprecated('Use itemDescriptor instead')
const Item$json = {
  '1': 'Item',
  '2': [
    {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    {'1': 'amount', '3': 3, '4': 1, '5': 5, '10': 'amount'},
    {'1': 'category', '3': 4, '4': 1, '5': 14, '6': '.Category', '10': 'category'},
    {'1': 'last_updated', '3': 5, '4': 1, '5': 9, '10': 'lastUpdated'},
    {'1': 'created_at', '3': 6, '4': 1, '5': 9, '10': 'createdAt'},
  ],
};

/// Descriptor for `Item`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDescriptor = $convert.base64Decode(
    'CgRJdGVtEg4KAmlkGAEgASgJUgJpZBIUCgV0aXRsZRgCIAEoCVIFdGl0bGUSFgoGYW1vdW50GA'
    'MgASgFUgZhbW91bnQSJQoIY2F0ZWdvcnkYBCABKA4yCS5DYXRlZ29yeVIIY2F0ZWdvcnkSIQoM'
    'bGFzdF91cGRhdGVkGAUgASgJUgtsYXN0VXBkYXRlZBIdCgpjcmVhdGVkX2F0GAYgASgJUgljcm'
    'VhdGVkQXQ=');

@$core.Deprecated('Use emptyDescriptor instead')
const Empty$json = {
  '1': 'Empty',
};

/// Descriptor for `Empty`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyDescriptor = $convert.base64Decode(
    'CgVFbXB0eQ==');

@$core.Deprecated('Use itemsDescriptor instead')
const Items$json = {
  '1': 'Items',
  '2': [
    {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.Item', '10': 'items'},
  ],
};

/// Descriptor for `Items`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemsDescriptor = $convert.base64Decode(
    'CgVJdGVtcxIbCgVpdGVtcxgBIAMoCzIFLkl0ZW1SBWl0ZW1z');

@$core.Deprecated('Use itemsByIdRequestDescriptor instead')
const ItemsByIdRequest$json = {
  '1': 'ItemsByIdRequest',
  '2': [
    {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
  ],
};

/// Descriptor for `ItemsByIdRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemsByIdRequestDescriptor = $convert.base64Decode(
    'ChBJdGVtc0J5SWRSZXF1ZXN0Eg4KAmlkGAEgASgJUgJpZA==');

@$core.Deprecated('Use itemByTodayRequestDescriptor instead')
const ItemByTodayRequest$json = {
  '1': 'ItemByTodayRequest',
  '2': [
    {'1': 'todayDate', '3': 1, '4': 1, '5': 9, '10': 'todayDate'},
  ],
};

/// Descriptor for `ItemByTodayRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemByTodayRequestDescriptor = $convert.base64Decode(
    'ChJJdGVtQnlUb2RheVJlcXVlc3QSHAoJdG9kYXlEYXRlGAEgASgJUgl0b2RheURhdGU=');

@$core.Deprecated('Use itemByCategoryResponseDescriptor instead')
const ItemByCategoryResponse$json = {
  '1': 'ItemByCategoryResponse',
  '2': [
    {'1': 'expenses', '3': 1, '4': 3, '5': 11, '6': '.Item', '10': 'expenses'},
    {'1': 'incomes', '3': 2, '4': 3, '5': 11, '6': '.Item', '10': 'incomes'},
  ],
};

/// Descriptor for `ItemByCategoryResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemByCategoryResponseDescriptor = $convert.base64Decode(
    'ChZJdGVtQnlDYXRlZ29yeVJlc3BvbnNlEiEKCGV4cGVuc2VzGAEgAygLMgUuSXRlbVIIZXhwZW'
    '5zZXMSHwoHaW5jb21lcxgCIAMoCzIFLkl0ZW1SB2luY29tZXM=');

@$core.Deprecated('Use itemsByResponseDescriptor instead')
const ItemsByResponse$json = {
  '1': 'ItemsByResponse',
  '2': [
    {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.Item', '10': 'items'},
  ],
};

/// Descriptor for `ItemsByResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemsByResponseDescriptor = $convert.base64Decode(
    'Cg9JdGVtc0J5UmVzcG9uc2USGwoFaXRlbXMYASADKAsyBS5JdGVtUgVpdGVtcw==');

